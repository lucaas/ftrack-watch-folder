# :coding: utf-8
# :copyright: Copyright (c) 2020 ftrack
import logging

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

import sync_folder_structure

class WatchFolderEventHandler(FileSystemEventHandler):
    '''Logs all the events captured.'''

    def __init__(self, root_path):
        super(WatchFolderEventHandler, self).__init__()
        self.root_path = root_path

    def on_moved(self, event):
        super(WatchFolderEventHandler, self).on_moved(event)

        what = 'directory' if event.is_directory else 'file'
        logging.info('Moved %s: from %s to %s', what, event.src_path,
                     event.dest_path)

    def on_created(self, event):
        super(WatchFolderEventHandler, self).on_created(event)

        what = 'directory' if event.is_directory else 'file'
        logging.info('Created %s: %s', what, event.src_path)
        if not event.is_directory:
            sync_folder_structure.upload_file(
                self.root_path,
                event.src_path
            )

    def on_deleted(self, event):
        super(WatchFolderEventHandler, self).on_deleted(event)

        what = 'directory' if event.is_directory else 'file'
        logging.info('Deleted %s: %s', what, event.src_path)

    def on_modified(self, event):
        super(WatchFolderEventHandler, self).on_modified(event)

        what = 'directory' if event.is_directory else 'file'
        logging.info('Modified %s: %s', what, event.src_path)


def watcher(root_path):
    event_handler = WatchFolderEventHandler(root_path)
    observer = Observer()
    observer.schedule(event_handler, root_path, recursive=True)
    observer.start()
    try:
        while observer.is_alive():
            observer.join(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()


