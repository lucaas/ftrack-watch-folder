# :coding: utf-8
# :copyright: Copyright (c) 2020 ftrack
import sys
import logging

import sync_folder_structure
import monitor

if __name__ == '__main__':
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S'
    )
    root_path = sys.argv[1] if len(sys.argv) > 1 else '.'

    sync_folder_structure.validate_folder_structure(root_path)
    monitor.watcher(root_path)
