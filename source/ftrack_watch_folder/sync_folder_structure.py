# :coding: utf-8
# :copyright: Copyright (c) 2020 ftrack
import logging
import os.path

import pathlib2 as pathlib
import ftrack_api.symbol

import shared_session

logger = logging.getLogger(__name__)

UPLOAD_ASSET_TYPE_ID = '8f4144e0-a8e6-11e2-9e96-0800200c9a66'

def query_path(session, path):
    '''Return Project or TypedContext query from list of folder names in *path*.'''
    if len(path) == 1:
        return session.query(
            'Project where name is "{}"'.format(path[0])
        )
    else:
        project = path[0]
        first_parent = path[1]
        query = 'name is "{}" and project.name is "{}"'.format(
            first_parent,
            project
        )
        for index, item in enumerate(path[2:]):
            query = 'name is "{}" and parent[TypedContext] has ({})'.format(
                item,
                query
            )
        return session.query(
            'TypedContext where {}'.format(query)
        )


def upload_file(root_directory, file_path):
    logger.info('Uploading file: {}'.format(file_path))
    if '.DS_Store' in file_path:
        return

    session = shared_session.get_shared_session()
    server_location = session.get(
        'Location',
        ftrack_api.symbol.SERVER_LOCATION_ID
    )

    relative_path = pathlib.Path(file_path).relative_to(root_directory)
    parents = relative_path.parent
    asset_name = relative_path.stem

    # TODO: Handle if parent does not exist.
    parent = query_path(session, str(parents).split('/')).one()

    existing_asset = session.query(
        'Asset where context_id is "{}" and name is "{}"'.format(
            parent['id'],
            asset_name
        )
    ).first()

    if existing_asset:
        logger.info('Versioning up existing asset')
        asset_id = existing_asset['id']
    else:
        logger.info('Creating new asset')
        new_asset = session.create('Asset', {
            'context_id': parent['id'],
            'name': asset_name,
            'type_id': UPLOAD_ASSET_TYPE_ID,
        })
        asset_id = new_asset['id']

    version = session.create('AssetVersion', {
        'asset_id': asset_id,
        'is_published': True,
    })
    session.commit()
    logger.info(u'Created version: {}'.format(version['id']))
    try:
        version.encode_media(file_path, keep_original=True)
        logger.info(u'File is uploaded and encoding')
    except Exception as error:
        logger.error(error)


def validate_folder_structure(root_directory):
    session = shared_session.get_shared_session()

    logger.info('Ensure root directory')
    pathlib.Path(root_directory).mkdir(parents=True, exist_ok=True) 

    logger.info('Creating project folders')
    projects = session.query(
        'select name from Project'
    )
    for project in projects:
        path = os.path.join(root_directory, project['name'])
        pathlib.Path(path).mkdir(parents=True, exist_ok=True) 


    logger.info('Creating TypedContext folders')
    contexts = session.query(
        'select project.name, link from TypedContext where object_type.is_leaf is False'
    )
    for context in contexts:
        folders = [item['name'] for item in context['link'][1:]]
        folders.insert(0, context['project']['name'])
        path = os.path.join(root_directory, *folders)
        pathlib.Path(path).mkdir(parents=True, exist_ok=True) 

    logger.info('Validated current project structure')
