# :coding: utf-8
# :copyright: Copyright (c) 2020 ftrack
import logging
import os.path
import json

import appdirs
import ftrack_api

_shared_session = None

logger = logging.getLogger(__name__)

def get_config_credentials():
    '''Return Connect credentials.'''
    config_file = os.path.join(
        appdirs.user_data_dir(
            'ftrack-connect', 'ftrack'
        ),
        'config.json'
    )

    config = None
    if os.path.isfile(config_file):
        logger.info(u'Reading config from {0}'.format(config_file))

        with open(config_file, 'r') as file:
            try:
                config = json.load(file)
            except Exception:
                logger.exception(
                    u'Exception reading json config in {0}.'.format(
                        config_file
                    )
                )

    credentials = None
    try:
        credentials = config['accounts'][0]
        server_url = credentials['server_url']
        api_user = credentials['api_user']
        api_key = credentials['api_key']
    except Exception:
        raise Exception('Failed to parse credentials from config data.')

    return credentials


def get_shared_session():
    '''Return shared ftrack_api session.'''
    global _shared_session

    if not _shared_session:
        credentials = get_config_credentials() 
        _shared_session = ftrack_api.Session(
        #    server_url=credentials['server_url'],
        #    api_key=credentials['api_user'],
        #    api_user=credentials['api_key'],
        )

    return _shared_session
