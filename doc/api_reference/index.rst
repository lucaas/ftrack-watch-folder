..
    :copyright: Copyright (c) 2020 ftrack

.. _api_reference:

*************
API reference
*************

ftrack_watch_folder
===============================

.. automodule:: ftrack_watch_folder

.. toctree::
    :maxdepth: 1
    :glob:

    */index
    *
